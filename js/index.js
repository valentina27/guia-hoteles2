$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval: 3000
	});

	$('#informacion').on('show.bs.modal', function(e){
	console.log('El modal Más Información se está mostrando');
	$('#infoBtn').removeClass('btn-outline-success');
	$('#infoBtn').addClass('btn-success');
	$('#infoBtn').prop('disabled', true);
	});

	$('#informacion').on('shown.bs.modal', function(e){
		console.log('El modal Más Información se mostro');
	});

	$('#informacion').on('hide.bs.modal', function(e){
		console.log('El modal Más Información se oculta');
	});

	$('#informacion').on('hidden.bs.modal', function(e){
		console.log('El modal Más Información se ocultó');
        $('#infoBtn').removeClass('btn-success');
        $('#infoBtn').addClass('btn-outline-success');
		$('#infoBtn').prop('disabled', false);
        
	});
});